type trajet = {
    bus : float;
    metro : float;
    tramway : float
} ;;

let duree trajet =
    trajet.bus +. trajet.metro +. trajet.tramway
;;

let vit_bus = 12. /. 60. ;;
let vit_metro = 30. /. 60. ;;
let vit_tramway = 20. /. 60. ;;

let distance trajet =
    trajet.bus *. vit_bus +. trajet.metro *. vit_metro +. trajet.tramway *. vit_tramway
;;

let em_bus = 140.;;
let em_metro = 4.1;; 
let em_tramway = 3.8;;

let emission trajet =
    let long_bus = trajet.bus *. vit_bus in
    let long_metro =  trajet.metro *. vit_metro in
    let long_tramway =  trajet.tramway *. vit_tramway in

    long_bus *. em_bus +.
    long_metro *. em_metro +.
    long_tramway *. em_tramway
;;

type critere = Duree | Distance | Emission ;;

let meilleur_trajet t1 t2 crit =
(* On part du principe qu'un trajet "bien" a une petite duree, peut parcourir de longue distance, et emet le moins de CO2 possible *)
    match crit with
    | Duree -> (duree t1) < (duree t2)
    | Distance -> (distance t1) > (distance t2)
    | Emission -> (emission t1) < (emission t2)

;;

let () = 
    let t1 = { bus = 60.; metro = 0. ; tramway = 0. } in 
    let t2 = { bus = 60.; metro = 60. ; tramway = 0. } in
    Printf.printf "%B" ( meilleur_trajet t1 t2 Duree )
;;


