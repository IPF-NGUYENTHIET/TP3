type interval = { inf : int; sup : int } ;;

let make_interval a b =
    if a < b then
        { inf = a; sup = b }
    else 
        { sup = a; inf = b }
;;

let max4 a b c d =
    max ( max a b ) ( max c d )
;;
let min4 a b c d =
    min ( min a b ) ( min c d )
;;

let add i1 i2 =
    let x = min4 (i1.inf + i2.inf) (i1.inf + i2.sup) (i1.sup + i2.inf) (i1.sup + i2.sup) in
    let y = max4 (i1.inf + i2.inf) (i1.inf + i2.sup) (i1.sup + i2.inf) (i1.sup + i2.sup) in
    { inf = x; sup = y }
;;

let sub i1 i2 =
    let x = min4 (i1.inf - i2.inf) (i1.inf - i2.sup) (i1.sup - i2.inf) (i1.sup - i2.sup) in
    let y = max4 (i1.inf - i2.inf) (i1.inf - i2.sup) (i1.sup - i2.inf) (i1.sup - i2.sup) in
    { inf = x; sup = y }
;;

let mul i1 i2 =
    let x = min4 (i1.inf * i2.inf) (i1.inf * i2.sup) (i1.sup * i2.inf) (i1.sup * i2.sup) in
    let y = max4 (i1.inf * i2.inf) (i1.inf * i2.sup) (i1.sup * i2.inf) (i1.sup * i2.sup) in
    { inf = x; sup = y }
;;

let contains_zero a =
    a.inf = 0 || a.sup = 0
;;

let div i1 i2 =
    if not ( contains_zero i1 || contains_zero i2 ) then (
        let x = min4 (i1.inf / i2.inf) (i1.inf / i2.sup) (i1.sup / i2.inf) (i1.sup / i2.sup) in
        let y = max4 (i1.inf / i2.inf) (i1.inf / i2.sup) (i1.sup / i2.inf) (i1.sup / i2.sup) in
        { inf = x; sup = y }
    ) else (
        raise Division_by_zero
    )
;;

let addv2 i1 i2 =
    {inf = i1.inf + i2.inf; sup = i1.sup + i2.sup}
;;

let subv2 i1 i2 =
    {inf = i1.inf - i2.sup; sup = i1.sup - i2.inf}
;;



let () =
    let test1 = make_interval 9 5 in
    let test2 = make_interval 4 2 in
    let test = sub test1 test2 in
    let testv2 = subv2 test1 test2 in
    Printf.printf "(%d, %d)" test.inf test.sup;
    Printf.printf "(%d, %d)" testv2.inf testv2.sup
;;


