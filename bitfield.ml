(* false_field : int ; false_field est un bit field associant false à tous ses bits *)
let false_field = 0 ;;

(* get : int -> int -> bool ; get f i renvoie la valeur (true ou false) du i-ème bit du bit field f *)
let get f i = f land (1 lsl i) <> 0 ;;

(* update : int -> int -> bool -> int ; update f i b renvoie un bit field dont tous les bits sont égaux à ceux de f, sauf le i-ème qui est égal à b. f est inchangé. *)
let update f i b = if b then f lor (1 lsl i) else f land (lnot (1 lsl i)) ;;

let rec all_true f n =
    if n = -1 then
        true
    else
        (get f n) && all_true f (n-1)
;;

let rec union f1 f2 n =


let () =
    Printf.printf "%B" (all_true 14 3)
;;
