let correct_size s =
    ( String.length s ) < 20
    ;;

let rec only_c c s =
    let l = String.length s in
    if (l > 0) then (
        (c = s.[0]) && only_c c (String.sub s 1 (l-1))
    ) else (
        true
    )
    ;;

let all_equals s =
    if ( (String.length s) > 0) then 
        only_c s.[0] s
    else
        true
    ;;

let rec equals a b =
    let la = String.length a in
    let lb = String.length b in

    if ( la = 0 || lb = 0 ) then 
        la = lb
    else
        a.[0] = b.[0] && equals (String.sub a 1 (la - 1)) (String.sub b 1 (lb-1))
    ;;

let rec is_palindrome s =
    let l = String.length s in
    if (l < 2 ) then
        true
    else
        s.[0] = s.[l-1] && is_palindrome (String.sub s 1 (l - 2))
    ;;

let rec is_substring_at i s1 s2 =
    match s1 with
    | "" -> true
    | _ -> s1.[0] = s2.[i] && is_substring_at (i+1) (String.sub s1 1 ((String.length s1) -1 )) s2
    ;;

let rec get_substring_index s1 s2 =
(* it wouldve been easier and less weird if passing an index as argument was allowed *)
    let l1 = String.length s1 in
    let l2 = String.length s2 in
    
    if l2 < l1 then (
        -1
    )
    else (
        if is_substring_at 0 s1 s2 then
            0
        else
            let tmp = 1 + get_substring_index s1 (String.sub s2 1 (l2 -1)) in
            if tmp > 0 then
                tmp
            else
                -1
    )
    ;;

let is_substring s1 s2 =
    (get_substring_index s1 s2) >= 0
    ;;

let () = 
    Printf.printf "%B" (is_substring "aaa" "aaaaammmkdqlqdzlq")
    ;;