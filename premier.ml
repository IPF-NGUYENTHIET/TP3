let premier n =
  if n <= 1 then
    Printf.printf "pas premier car inférieur ou égal à 1\n"
  else
    let rec aux a b =
      if ( a = 1 ) then (
        Printf.printf "premier \n";
      ) else (
        if (b mod a = 0) then (
          Printf.printf "pas premier, divisibile par %d" a
        )
        else (
          aux (a-1) b
        )
      )
    in
    aux (n-1) n
;;

let premierv2 n =
  if n <= 1 then
    Printf.printf "pas premier car inférieur ou égal à 1\n"
  else
    let rec aux a b =
      if ( 4 <= (a*a) && (a*a) <= b ) then (
        if (b mod a = 0) then (
          Printf.printf "pas premier, divisibile par %d" a
        ) else (
          aux (a+1) b
        )
      )
      else
      (
        Printf.printf "premier"
      )
    in
    aux 2 n
;;

let rec main () = 
  let n = read_int () in
  premier n;
  main ()
;;

let () =
  main()
;;
